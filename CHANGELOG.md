# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.3](https://gitlab.coko.foundation/cokoapps/service-auth/compare/v2.0.2...v2.0.3) (2025-02-04)


### Bug Fixes

* bump coko server version to avoid conflicts in services ([bfb2ab6](https://gitlab.coko.foundation/cokoapps/service-auth/commit/bfb2ab6f9899311c8c27c2136d66079517abcc01))

### [2.0.2](https://gitlab.coko.foundation/cokoapps/service-auth/compare/v2.0.1...v2.0.2) (2024-12-02)


### Bug Fixes

* process exit on end of create:client script ([8612dca](https://gitlab.coko.foundation/cokoapps/service-auth/commit/8612dcabcd820769c1e2a17703e0d0aa08a8e322))

### [2.0.1](https://gitlab.coko.foundation/cokoapps/service-auth/compare/v2.0.0...v2.0.1) (2024-09-24)


### Bug Fixes

* fix bcrypt import ([c0c65d2](https://gitlab.coko.foundation/cokoapps/service-auth/commit/c0c65d24e94eae3e75d1c2a296fbe9f2ee6735de))

## [2.0.0](https://gitlab.coko.foundation/cokoapps/service-auth/compare/v1.2.0...v2.0.0) (2024-09-24)


### Features

* upgrade to work with coko server v4 ([2c2f408](https://gitlab.coko.foundation/cokoapps/service-auth/commit/2c2f408ed3da772c4f19eaadbe3b23d7278d7e59))

## [1.2.0](https://gitlab.coko.foundation/cokoapps/service-auth/compare/v1.1.7...v1.2.0) (2024-01-30)


### Features

* **service:** create service client from env ([184c5b0](https://gitlab.coko.foundation/cokoapps/service-auth/commit/184c5b0f004c180998ef138c6ff2e0825ad518af))

### [1.1.7](https://gitlab.coko.foundation///compare/v1.1.6...v1.1.7) (2021-03-31)

### [1.1.6](https://gitlab.coko.foundation///compare/v1.1.5...v1.1.6) (2020-12-09)

### [1.1.5](https://gitlab.coko.foundation///compare/v1.1.4...v1.1.5) (2020-11-16)


### Bug Fixes

* **service:** missing {} added ([960a2bc](https://gitlab.coko.foundation///commit/960a2bca0af5cfcf32275c5a26e1c4bad7b8133b))

### [1.1.4](https://gitlab.coko.foundation///compare/v1.1.3...v1.1.4) (2020-11-16)


### Bug Fixes

* **service:** remove logic of manual client creation ([a4db23c](https://gitlab.coko.foundation///commit/a4db23c0903f861137904ee3b55f8d1770620def))

### [1.1.3](https://gitlab.coko.foundation///compare/v1.1.2...v1.1.3) (2020-11-12)

### [1.1.2](https://gitlab.coko.foundation///compare/v1.1.1...v1.1.2) (2020-11-12)

### [1.1.1](https://gitlab.coko.foundation///compare/v1.1.0...v1.1.1) (2020-10-29)


### Bug Fixes

* **service:** delete bcrypt and replaced with bcryptjs ([c0c1213](https://gitlab.coko.foundation///commit/c0c12132a7880be46bb74e0f98feef0bc3f7c03d))

## 1.1.0 (2020-10-29)


### Features

* **service:** auth endpoint added ([5cce662](https://gitlab.coko.foundation///commit/5cce662635c6682750de1b5da58488e1ab4b0d82))


### Bug Fixes

* **service:** header checks ([8cbb690](https://gitlab.coko.foundation///commit/8cbb6901103a485152f95508f63fabe9427070cf))
* **service:** service auth complete ([1032452](https://gitlab.coko.foundation///commit/1032452487cca1fc336f012d91847c863521084e))

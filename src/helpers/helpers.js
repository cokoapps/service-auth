const jwt = require('jsonwebtoken')
const config = require('config')

const { models } = require('../models')

const { ServiceClient } = models

const createJWT = data => {
  let expiresIn = 28800000 // 8hours

  if (config.has('tokenExpiresIn')) {
    expiresIn = config.get('tokenExpiresIn')
  }

  if (!config.get('secret')) {
    throw new Error('secret is required')
  }

  return jwt.sign(
    {
      data,
    },
    config.get('secret'),
    { expiresIn },
  )
}

const verifyJWT = token => {
  try {
    if (!config.get('secret')) {
      throw new Error('secret is required')
    }

    return jwt.verify(token, config.get('secret'))
  } catch (err) {
    throw new Error(err)
  }
}

const authenticate = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const decodedToken = verifyJWT(token)
    const { data } = decodedToken
    const { clientId } = data
    const client = await ServiceClient.query().findById(clientId)

    if (!client) {
      throw new Error('client does not exist')
    }

    return next()
  } catch (e) {
    const msg =
      e.message === 'TokenExpiredError: jwt expired'
        ? 'expired token'
        : 'invalid request'

    return res.status(401).json({
      msg,
    })
  }
}

module.exports = { createJWT, verifyJWT, authenticate }
